Exercício de Programação 2

Programa passado pelo professor Renato Coral pela disciplina Orientação a Objeto
que observar a compreensão prática do aluno no decorrer da matéria.

O EP2 tem como objetivo principal "implementar um programa em Java com interfaces 
gráficas capaz de ler arquivos de imagens nos formatos PGM e PPM e extrair informações
embutidas nestas imagens através da técnica da esteganografia. 
Além disso, o programa também deverá aplicar os seguintes filtros nas imagens pgm: 
1. Negativo. 
2. Smooth. 
3. Sharpen."

Como compilar o EP2:
-Abrir o terminal.
-git init/git clone https://gitlab.com/vinicinolivera/EP2.git.
-Você terá que ter em sua Máquina o IDE Netbeans.
-Ao abrir o IDE você ira em "Arquivo" e em "Abrir Projeto", logo após irá procurar
a pasta do projeto e abrir.
-Em "Executar", você clicará em Executar Projeto.

Autor: 
Vinícius Rodrigues Oliveira
14/0165291
