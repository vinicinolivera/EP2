package ep2;

import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.JOptionPane;

public class DecifrarMSG_PGM extends Imagem {

    private int[][] pixels;
    private byte[] bytes;

    DecifrarMSG_PGM(String nome) {
        this.montarImagem(nome);
    }

    private void setPixels(int height, int width) {
        this.pixels = new int[height][width];
    }

    private void setPixel(int y, int x, int color) {
        this.pixels[y][x] = color;
    }
    
    public int getPixel(int y, int x){
        return this.pixels[y][x];
    }

    private void setBytes() {
        this.bytes = new byte[this.getSize()];
    }

    private void setByte(int pos, byte bb) {
        this.bytes[pos] = bb;
    }

    public byte getByte(int pos) {
        return this.bytes[pos];
    }

    /**
     *
     * @param path
     */
    @Override
    public void montarImagem(String nome) {
        this.open(nome, BufferedImage.TYPE_INT_RGB);
        this.setPixels(this.getHeight(), this.getWidth());
        this.setBytes();

        for (int y = 0; y < this.getHeight(); y++) {
            for (int x = 0; x < this.getWidth(); x++) {
                try {
                    int color = this.getArquivo().read();
                    byte bb = (byte) color;
                    this.setByte(this.getWidth() * y + x, bb);                   

                    if (color < 0 || color > this.getMax()) {
                        color = 0;
                    }

                    //System.out.println(pos++);
                    this.setPixel(y, x, color);
                    this.getImagem().setRGB(x, y, new Color(color, color, color).getRGB());
                } catch (Throwable t) {
                    t.printStackTrace(System.err);
                }
            }
        }
        this.getMessage();
    }

    public void getMessage() {
        char character = 0;
        int count = 1;
        // get start position
        int start = Integer.parseInt(this.getComentario().replaceAll("[\\D]", ""));
        char b[] = null;
        int teste = 0;
        String minhaString1 = "";

        for (int pos = start; pos < this.getSize(); pos++) {
            character <<= 1;
            character |= this.getByte(pos) & 0x01;
            if (count == 8) {             // Is it the least significant bit ? 
                if (character == '#') {   // Is it the end of the message ?
                    break;
                }
                
                minhaString1 += (char) character;
                count = 0;
                character = 0;
//                minhaString1 = String.valueOf(character);
//                System.out.print(minhaString1);
//                count = 0;                // RESET COUNT
//                character = 0;            // RESET CHAR
            }
            count++;
        }
//        int resposta = 0;
//        if (resposta == JOptionPane.YES_OPTION) {
            // verifica se o usuário clicou no botão YES
            JOptionPane.showMessageDialog(null,minhaString1);
//        } else {
//            JOptionPane.showMessageDialog(null, minhaString1);
//        }
        //}        
    }
    }

