package ep2;


import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ImagemPPM extends Imagem {

    private int[][][] pixels;

    ImagemPPM(String nome) {
        this.montarImagem(nome);
    }

    private void setPixels(int height, int width) {
        this.pixels = new int[height][width][3];
    }

    // Fill every column of a pixel.
    private void setPixel(int y, int x, int c, int value) {
        this.pixels[y][x][c] = value;
    }

    // Get the array of pixel
    public int[] getPixel(int y, int x) {
        return this.pixels[y][x];
    }

    /**
     *
     * @param nome
     */
    @Override
    public void montarImagem(String nome) {
        this.open(nome, BufferedImage.TYPE_INT_RGB);
        this.setPixels(this.getHeight(), this.getWidth());


        int y = 0;
        int x = 0;

        for (y = 0; y < this.getHeight(); y++) {
            for (x = 0; x < this.getWidth(); x++) {
                try {
                    int red = this.getArquivo().read();
                    int green = this.getArquivo().read();
                    int blue = this.getArquivo().read();

                    //System.out.println(red+" "+green+" "+blue);
                    this.setPixel(y, x, 0, red);
                    this.setPixel(y, x, 1, green);
                    this.setPixel(y, x, 2, blue);

                    Color pixel = new Color(red, green, blue);
                    this.getImagem().setRGB(x, y, pixel.getRGB());
                } catch (Throwable t) {
                    t.printStackTrace(System.err);
                }
            }
        }
        JFrame frame = new JFrame();
	frame.getContentPane().setLayout(new FlowLayout());
//      frame.getContentPane().add(new JLabel(new ImageIcon(this.getArquivo())));
	frame.getContentPane().add(new JLabel(new ImageIcon(this.getImagem())));
	frame.pack();
	frame.setVisible(true);
    }

    public void applyFilterRGB(int color) {
        int y = 0;
        int x = 0;
        for (y = 0; y < this.getHeight(); y++) {
            for (x = 0; x < this.getWidth(); x++) {
                int[] rgb = new int[3];
                System.arraycopy(this.pixels[y][x], 0, rgb, 0, rgb.length);  // copy pixel
                switch(color){
                    case 0:
                        rgb[1] = 0;
                        rgb[2] = 0;
                        break;
                    case 1:
                        rgb[0] = 0;
                        rgb[2] = 0;
                        break;
                    case 2:
                        rgb[1] = 0;
                        rgb[0] = 0;
                        break;
                        
                }
                Color pixel = new Color(rgb[0], rgb[1], rgb[2]);
                this.getImagem().setRGB(x, y, pixel.getRGB());
            }
        }
    }
}
