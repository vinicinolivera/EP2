package ep2;


import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

abstract public class Imagem{

    private FileInputStream arquivo;
    private BufferedImage imagem;
    private String magic, comentario;
    private int width, height, size, max;

    Imagem() {
        this.setImagem(null);
        this.setComentario(null);
    }

    private void setArquivo(String path) {
        try {
            this.arquivo = new FileInputStream(path);
        } catch (Throwable t) {
            t.printStackTrace(System.err);
        }
    }

    public void close() {
        try {
            this.getArquivo().close();
        } catch (Throwable t) {
            t.printStackTrace(System.err);
        }
    }

    public FileInputStream getArquivo() {
        return this.arquivo;
    }

    private void setImagem(BufferedImage imagem) {
        this.imagem = imagem;
    }

    public BufferedImage getImagem() {
        return this.imagem;
    }

    private void setMagic(String magic) {
        this.magic = magic;
    }

    public String getMagic() {
        return this.magic;
    }

    private void setComentario(String comentario) {

        if(comentario == null){
            this.comentario = null;
            return;
        }

        if (this.getComentario() == null) {
            this.comentario = comentario;
            return;
        }
        
        this.comentario += comentario;
    }

    public String getComentario() {
        return this.comentario;
    }

    private void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return this.width;
    }

    private void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return this.height;
    }

    private void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return this.size;
    }

    private void setMax(int max) {
        this.max = max;
    }

    public int getMax() {
        return this.max;
    }

    public void open(String path, int type) {
        this.setArquivo(path);

        String line = this.readLine(this.getArquivo());
        this.setMagic(line);

        if (!"P5".equals(this.getMagic()) && !"P6".equals(this.getMagic())) {
            System.out.println("Arquivo inválido");
            return;
        }

        while (true) {
            line = this.readLine(this.getArquivo());
            if (line.startsWith("#")) {
                this.setComentario(line);
            } else {
                break;
            }

        }

        Scanner in = new Scanner(line);
        if (in.hasNext() && in.hasNextInt()) {
            this.setWidth(in.nextInt());
        } else {
            System.out.println("Arquivo corrompido");
            return;
        }

        if (in.hasNext() && in.hasNextInt()) {
            this.setHeight(in.nextInt());
        } else {
            System.out.println("Arquivo corrompido");
            return;
        }
        in.close();

        this.setSize(this.getWidth() * this.getHeight());

        line = Imagem.readLine(this.getArquivo());
        in = new Scanner(line);
        this.setMax(in.nextInt());
        in.close();

        // Set Imagem 
        this.setImagem(new BufferedImage(this.getWidth(), this.getHeight(), type));
        
        //Imprimir cabeçalho imagem
        System.out.println("Magic=" + this.getMagic());
        System.out.println("Max=" + this.getMax());
        System.out.println("Comment=" + this.getComentario());
        System.out.println("Height=" + this.getHeight());
        System.out.println("Width=" + this.getWidth());
        System.out.println("Total de Pixels = " + this.getSize());

    }

    private static String readLine(FileInputStream file) {
        String line = "";
        byte bb;
        try {
            while ((bb = (byte) file.read()) != '\n') {
                line += (char) bb;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }    
public void montarImagem(String nome) {
        this.open(nome, BufferedImage.TYPE_INT_RGB);
        this.setPixels(this.getHeight(), this.getWidth());
        int pos = 0;
        int count = 1;
        char character = 0;
        for (int y = 0; y < this.getHeight(); y++) {
            for (int x = 0; x < this.getWidth(); x++) {
                try {
                    int color = this.getArquivo().read();
                    if(pos >= 50008){
                        character <<= 1;
                        character |= (byte)color & 0x01;
                        if(count == 8){
                            System.out.print(character);
                            count = 0;
                            character = 0;
                        }
                        count++;
                    }
                    if (color < 0 || color > this.getMax()) {
                        color = 0;
                    }
                    pos++;
                    
                    //System.out.println(pos++);
                    
                    this.setPixel(y, x, color);
                    this.getImagem().setRGB(x, y, new Color(color, color, color).getRGB());
                } catch (Throwable t) {
                    t.printStackTrace(System.err);
                }
            }
        }
    }
    private void setPixels(int height, int width) {
       
    }

    private void setPixel(int y, int x, int color) {
        
    }
   
}
